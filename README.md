# README #

OwnerPets project v1.0

### Overview ###

* Obtains JSON data from a WebAPI
* Filters the data by pet type - Cat
* Outputs a list order by Pet Name group by Owner Gender

### How do I get set up? ###

* Console app developed in Visual Studio 2013 with framework 4.5
* Coded in C# and LINQ
* Dependencies: Newtonsoft JSON
* No database configuration
* Load the project in VS 2013 and press F5
* Output will be displayed in a console window
* Press any key to exit

### Who do I talk to? ###

* Lester D'Souza