﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;

namespace OrderPets
{
    #region Class Objects

    /// <summary>
    /// Person class object
    /// </summary>
    class Person
    {
        public string name { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        public Pet[] pets { get; set; }
    }
    /// <summary>
    /// Pet class object
    /// </summary>
    class Pet
    {
        public string name { get; set; }
        public string type { get; set; }
    }
    /// <summary>
    /// Person Pet class object
    /// </summary>
    class PersonPet
    {
        public string persongender { get; set; }
        public string petname { get; set; }
    }

    #endregion

    class Program
    {
        static void Main(string[] args)
        {
            /// get json data
            string JSONData = getJSONData("http://agl-developer-test.azurewebsites.net/people.json");

            /// get list of persons from the json data
            List<Person> persons = getPersonObject(JSONData);

            /// write out the data to console filtered by an animal
            WriteOutData(persons, "cat");

            /// pause and wait for key press
            Console.ReadKey();
        }

        /// <summary>
        /// Writes out object data to console
        /// </summary>
        /// <param name="persons">list of Persons</param>
        /// <param name="filterby">string value to filterby</param>
        static void WriteOutData(List<Person> persons, string filterby)
        {
            try  
            {
                /// linq query to get owner gender and pet name
                var qryPrPt = from pr in persons
                        /// eliminate owners with no pets
                        where pr.pets != null
                        from pt in pr.pets
                        /// filterby type
                        where pt.type.ToLower() == filterby.ToLower()
                        /// order by owner gender then pet name
                        orderby pr.gender descending, pt.name
                        select new PersonPet() { persongender = pr.gender, petname = pt.name };
                
                /// linq query to group by owner gender
                var qryRslt = from prpt in qryPrPt
                         group prpt by prpt.persongender;

                /// output the result
                foreach (var rslt in qryRslt)
                {
                    Console.WriteLine(rslt.Key);
                    foreach (PersonPet pp in rslt)
                    {
                        Console.WriteLine("\t" + pp.petname);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// getting a person object data collection based on string JSON data 
        /// </summary>
        /// <param name="data">string JSON data</param>
        /// <returns>list of persons</returns>
        static List<Person> getPersonObject(string data)
        {
            List<Person> persons = null;
            try
            {
                /// deserialize the json object into a list of persons
                persons = JsonConvert.DeserializeObject<List<Person>>(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            return persons;
        }

        /// <summary>
        /// get JSON data from web api
        /// </summary>
        /// <param name="URL">string web api URL</param>
        /// <returns>string JSON data</returns>
        static string getJSONData(string URL)
        {
            string data = "";
            HttpClient client = new HttpClient();
            HttpResponseMessage response = null;
            try
            {
                /// get json data using httpclient
                response = client.GetAsync(new Uri(URL)).Result;
                if (response.IsSuccessStatusCode)
                {
                    /// reading the data as a string
                    data = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                client.Dispose();
                client = null;
                response.Dispose();
                response = null;
            }

            return data;
        }
    }
}

